# PravdaNews

##### Что если бы Советский Союз не распался и советский народ окунулся в бескрайние просторы интернета? 
##### Попасть в альтернативную историю позволит приложение PravdaNews: по мнению автора, именно так выглядело бы советское приложение для новостей!

## Внимание, для смены темы нужно кликнуть на звезду :3
##### (Для перехода в закладки сверху слева кнопка)

## Скриншоты
Темная тема, главный экран

<img alt="img" src="readme_assets/image.png" height="400" />

Светлая тема, главный экран

<img alt="img" src="readme_assets/image-1.png" height="400" />

Темная тема, второй экран

<img alt="img" src="readme_assets/image-2.png" height="400" />

Светлая тема, второй экран

<img alt="img" src="readme_assets/image-3.png" height="400" />


Светлая тема, экран закладок

<img alt="img" src="readme_assets/image-4.png" height="400" />

Темная тема, экран закладок

<img alt="img" src="readme_assets/image-5.png" height="400" />



## Описание проекта


Домашка 3
- Анимация открытия новости (Tween)
- Анимация открытия любимых новостей (Tween)
- Анимация кнопки смены темы (крутится звезда, AnimationController)
- Hero для картинки новости
- SilverHeader для строки поиска новостей
- CI/CD github
- новые скриншоты

Домашка 2
- Riverpod для state менеджмента
- Dependency Injection с помощью riverpod
- Разделена бизнес-логика и UI
- Написаны два Unit теста для Api
- Полностью реализован механизм сохраннеия новостей – сохранение в БД Sqlite, удаление
- Реализован новый экран с сохраненными новостями
- Базовые требования (APK, скриншоты, описание)

Домашка 1
- Реализован основной экран с новостями
- Реализована система бесконечной прокрутки (на основе пагинации API)
- Поддержана светлая и темная темы
- Реализован второй экран с отдельной новостью и ссылкой на полную статью
- Pull to refresh на страничке с новостями
- flutter_lints подключен, не дает ошибок, по крайней мере у меня на компьютере)))
- Сериализация json в объект без сторонних библиотек
- Код отформатирован
- APK файлы в наличии